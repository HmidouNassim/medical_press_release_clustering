# -*- coding: utf-8 -*-
import datetime
from datetime import date, timedelta
import logging, argparse, pickle, os
from pathlib import Path
from dotenv import find_dotenv, load_dotenv

def call_period(end_date, start_date='2017-1-1'):
    """ Returns a list of date between start and end date provided
    by argparse. Elements of this list are used as parameters for
    Benzinga Pro API.
    """

    year_month_day_start = start_date.split('-')
    year_month_day_end = end_date.split('-')
    d1 = date(int(year_month_day_start[0]), int(year_month_day_start[1]), int(year_month_day_start[2]))  # start date
    d2 = date(int(year_month_day_end[0]), int(year_month_day_end[1]), int(year_month_day_end[2]))  # end date

    delta = d2 - d1 # timedelta

    day_list = []

    for i in range(delta.days + 1):
        date = d1 + timedelta(days=i)
        year = str(date.year)#année

        if len(str(date.month)) == 1: #mois
            month = '0' + str(date.month)
        else:
            month = str(date.month)

        if len(str(date.day)) == 1: #jour
            day = '0' + str(date.day)
        else:
            day = str(date.day)

        day_list.append( year + '-' + month + '-' + day)
    return day_list

def api_call(token_key, symbol_list, day_list):
    """
    Function use to call Benzinga API providing press release data.
    params :
    - token_key : private token for benzinga API. Provided by environment
                  variable.
    - symbol_list : Nasdaq symbols of interest. Provided by environment
                    variable.
    - day_list : list of date to call. Provided by call_period function.
    """

    #The API limitation is 1 call per second. No map-async possibility here.
    medical_press_release_data = []

    for date_to_scrape in day_list:
        for x in range(0,100):
            params = (('token', token_key),('date', date_to_scrape),('pageSize', 100),('displayOutput','full'),('page',x))

            response = requests.get('http://api.benzinga.com/api/v2/news', headers={'accept': 'application/json'}, params=params)
            response_call = response.json()

            #on vérifie que l'api call n'est pas vide
            if len(response_call) == 0:
                break

            for x in range(0,len(response_call)):
                if (response_call[x]['stocks'] != [])  and (len(response_call[x]['stocks']) == 1) and (response_call[x]['stocks'][0]['name'].lower() in symbols):
                    medical_press_release_data.append(response_call[x])

    return medical_press_release_data


def main(start_date, end_date, token_key, project_dir, logger):

    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """

    #create day list to call
    day_list = call_period(end_date, start_date)
    logger.info('creation of day_list from ' + str(start_date) + ' to ' + str(end_date))

    #Call Benzinga API
    with open('symbols.pkl', 'rb') as handle:
        symbols = pickle.load(handle)
    logger.info('Load symbol list : ' + str(symbols))

    logger.info('Beggining of API calls')
    medical_press_release_data = api_call(token_key, symbols, day_list)
    logger.info('End of API calls')

    #Save the list as a pickle
    with open(project_dir + '/data/raw/press_release_data.pkl', 'wb') as r:
        pickle.dump(medical_press_release_data, r)
    logger.info('save press_release_data.pkl')

if __name__ == '__main__':

    # creation of logger object
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')

    # get project dir to save pickle data in /data/raw/ directory
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automatically by walking up directories until it's found,
    # then load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    token_key = os.environ.get("TOKEN_KEY")

    # defined command line options and parse the command line
    CLI=argparse.ArgumentParser()
    CLI.add_argument("--start_date", type=str, default='2017-01-01')
    today=str(datetime.datetime.now().year) + '-' + str(datetime.datetime.now().month) + '-' + str(datetime.datetime.now().day)
    CLI.add_argument("--end_date", type=str, default=today)

    args = CLI.parse_args()
    start_date = args.start_date
    end_date = args.end_date

    #exec main
    main(start_date, end_date, token_key, project_dir, logger)
