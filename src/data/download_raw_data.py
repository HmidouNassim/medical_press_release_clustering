import requests, pickle, argparse, logging
from pathlib import Path
from logging.handlers import RotatingFileHandler

def creation_logs():

    """ création de l'objet logger et handler qui vont sauvegarder les log
    dans un fichier et les afficher dans le terminal """

    #création de l'objet logger avec un niveau DEBUG
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    #création de l'objet formatter
    formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')

    #création du fichier dans lequel on stock les logs
    log_file_name = 'download_raw_data.log'

    #création d'un handler qui écrit les logs en mode 'append' et une taille max de fichier
    file_handler = RotatingFileHandler(log_file_name, 'a', 1000000000, 100)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    # Second handler pour rediriger les logs sur la console
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.DEBUG)
    stream_handler.setFormatter(formatter)

    if (logger.hasHandlers()):
        logger.handlers.clear()
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    return logger, file_handler, stream_handler

def is_downloadable(url):
    """
    Does the url contain a downloadable resource
    """
    h = requests.head(url, allow_redirects=True)
    header = h.headers
    content_type = header.get('content-type')
    if 'text' in content_type.lower():
        return False
    if 'html' in content_type.lower():
        return False
    return True


if __name__ == '__main__':

    #2) Création du logger
    logger, file_handler, stream_handler = creation_logs()

    # defined command line options
    CLI=argparse.ArgumentParser()
    CLI.add_argument("--url", type=str)

    # parse the command line
    args = CLI.parse_args()
    url = args.url

    if url is not None:
        logger.info('url to download : ' + url)

        #'https://www.cs.cmu.edu/~enron/enron_mail_20150507.tar.gz'
        if is_downloadable(url):
            #get output file name
            if url.find('/') != -1: #Contains given substring
                output_filename = url.rsplit('/', 1)[1]
            else:
                output_filename = 'raw_data'
            logger.debug('output filename : ' + str(output_filename))

            #download raw data
            logger.info('Beginning file download with requests')
            r = requests.get(url, allow_redirects=True)

            # get project dir to save pickle data in /data/raw/ directory
            project_dir = Path(__file__).resolve().parents[2]
            logger.debug('project directory : ' + str(project_dir))

            #save file in data/raw/
            logger.info('Save downloaded file in data/raw/')
            with open(str(project_dir) + '/data/raw/' + str(output_filename), 'wb') as f:
                pickle.dump(r.content, f)

            #with open('/Users/scott/Downloads/cat3.jpg', 'wb') as f:
                #f.write(r.content)

            # Retrieve HTTP meta-data
            #logger.info(r.status_code)
            #logger.info(r.headers['content-type'])
            #logger.info(r.encoding)

        else:
            logger.warning('url is not downloadable. Please provide a valid url')
    else:
        logger.warning('url is not downloadable. Please provide a valid url')

