import requests, pickle, argparse, logging, tarfile
from pathlib import Path
from logging.handlers import RotatingFileHandler

class Download_raw_data():
    """ This class allow to download a provided url,
    Unzip the downloaded file if necessary and save it in /data/raw/ """

    def creation_logs(self):
        """ création de l'objet logger et handler qui vont sauvegarder les log
        dans un fichier et les afficher dans le terminal """

        #création de l'objet logger avec un niveau DEBUG
        logger = logging.getLogger()
        logger.setLevel(logging.DEBUG)
        #création de l'objet formatter
        formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')

        #création du fichier dans lequel on stock les logs
        log_file_name = 'download_raw_data.log'

        #création d'un handler qui écrit les logs en mode 'append' et une taille max de fichier
        file_handler = RotatingFileHandler(log_file_name, 'a', 1000000, 100)
        file_handler.setLevel(logging.DEBUG)
        file_handler.setFormatter(formatter)
        # Second handler pour rediriger les logs sur la console
        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(logging.DEBUG)
        stream_handler.setFormatter(formatter)

        if (logger.hasHandlers()):
            logger.handlers.clear()
        logger.addHandler(file_handler)
        logger.addHandler(stream_handler)

        return logger, file_handler, stream_handler

    def is_downloadable(self, url):
        """ Does the url contain a downloadable resource """

        h = requests.head(url, allow_redirects=True)
        header = h.headers
        content_type = header.get('content-type')
        if 'text' in content_type.lower():
            return False
        if 'html' in content_type.lower():
            return False
        return True

    def save_raw_data(self, request_file, output_filename, logger):
        """ Check if raw data is an archive or zipped file.
        Unzip it if it's the case, otherwise save it directly """

        # get project dir to save pickle data in /data/raw/ directory
        project_dir = Path(__file__).resolve().parents[2]
        logger.debug('project directory : ' + str(project_dir))

        #check if downloaded file is a tar or tar.gz
        if (output_filename.endswith("tar.gz")) or (output_filename.endswith("tar")):
            #save file .tar or .tar.gz file in data/raw/
            logger.info('Save .tar or .tar.gz file in data/raw/')
            with open(str(project_dir) + '/data/raw/' + str(output_filename), 'wb') as f:
                pickle.dump(request_file.content, f)

            try:
                tar = tarfile.open(str(project_dir) + '/data/raw/' + str(output_filename), "r")
                tar.extractall()
            except:
                logger.warning('Unable to extract data from .tar or .tar.gz. Check your url and run pip3 install tarfile --upgrade')
            else:
                #save unziped file in data/raw/
                logger.info('Unzip file in data/raw/')
                with open(str(project_dir) + '/data/raw/', 'wb') as f:
                    pickle.dump(tar, f)

        else:
            #save file in data/raw/
            logger.info('Save downloaded file in data/raw/')
            with open(str(project_dir) + '/data/raw/' + str(output_filename), 'wb') as f:
                pickle.dump(request_file.content, f)

    def main(self):
        """ Download the data """

        #Création du logger
        logger, file_handler, stream_handler = self.creation_logs()

        # defined command line options
        CLI=argparse.ArgumentParser()
        CLI.add_argument("--url", type=str)

        # parse the command line
        args = CLI.parse_args()
        url = args.url

        if url is not None:
            logger.info('url to download : ' + url)

            #'https://www.cs.cmu.edu/~enron/enron_mail_20150507.tar.gz'
            if self.is_downloadable(url):
                #get output file name
                if url.find('/') != -1: #Contains given substring
                    output_filename = url.rsplit('/', 1)[1]
                else:
                    output_filename = 'raw_data'
                logger.debug('output filename : ' + str(output_filename))

                #download raw data
                logger.info('Beginning file download with requests')
                r = requests.get(url, allow_redirects=True)

                #save the downloaded data
                logger.info('Save downloaded file in /data/raw/ and unzip it if necessary')
                self.save_raw_data(r, output_filename, logger)

            else:
                logger.warning('url is not downloadable. Please provide a valid url')
        else:
            logger.warning('url is not downloadable. Please provide a valid url')

if __name__ == '__main__':
    enron = Download_raw_data()
    enron.main()
